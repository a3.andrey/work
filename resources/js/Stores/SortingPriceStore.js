import {defineStore} from "pinia";
import {ref} from "vue";
export const UseSortingPrice = defineStore('UseSortingPrice',() => {
    const  sortPosition = ref(0)
    const  is = true
    const  item = ref({
        0:'Дешевле',
        1:'Дороже',
    })

    return {
        sortPosition,
        is,
        item,
    }
});
