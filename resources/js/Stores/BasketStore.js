import {defineStore} from "pinia";
import {computed, ref,onMounted } from "vue";
import _ from "lodash";

export const UseBasket = defineStore('UseBasket',() => {

    const products = ref([])

    const count = computed(() => {
        return _.sumBy(products.value, 'quantity' );
    })

    const total = computed(() => {
        let total = 0;
         _.forEach(products.value, function(product) {
             total += product.price*product.quantity
        });

        return Math.round(total * 100) / 100 ;
    })

    const items = computed(() => {
        return products.value;
    })

    const clearAllCart = () =>  {
        products.value = [];
        updateLacalStorage(products.value)
    }

    const addToCart = (product) => {

        let item = products.value.find(i => Number(i.id) === Number(product.id))
        if (item){
            if(item.quantity + 1 < product.count){
                item.quantity++
            }
        } else{
            products.value.push({...product, quantity:1})
        }

        updateLacalStorage(products.value)
    }

    const removeFromCart = (product) => {
        let item = products.value.find(i => i.id === product.id)

        if (item){
            if(item.quantity > 1){
                item.quantity--
            }else{
                products.value = products.value.filter( i => i.id !== product.id)
            }
        }

        updateLacalStorage(products.value)
    }

    const deleteItemCart = (product) => {
        let item = products.value.find(i => i.id === product.id)
        if (item){
            products.value = products.value.filter( i => i.id !== product.id)
        }
        updateLacalStorage(products.value)
    }

    const updateLacalStorage = (products) => {
        localStorage.setItem('cart',JSON.stringify(products))
    }

    const updateCartFromLocalStorage = () => {
        const cart = localStorage.getItem('cart')
        if (cart){
            products.value = JSON.parse(cart)
        }
    }

    updateCartFromLocalStorage()

return {
    addToCart,
    count,
    removeFromCart,
    items,
    deleteItemCart,
    total,
    clearAllCart
    }

});






