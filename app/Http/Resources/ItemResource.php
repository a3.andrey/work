<?php

namespace App\Http\Resources;

use App\Models\Item;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'item' => $this->item,
            'name' => $this->name,
            'game' => $this->game,
            'price' => $this->price,
            'quantity' => $this->products->count(),
            'image' => $this->image,
            'categories' => $this->categories->pluck('id'),
            'textFeatures' => $this->textFeatures,
            'iconFeatures' => $this->iconFeatures,
            'colorFeatures' => $this->colorFeatures,
            'numberFeatures' => $this->numberFeatures,
        ];
    }
}
