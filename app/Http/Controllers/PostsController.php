<?php


namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Inertia\Inertia;

class PostsController extends Controller
{
    public function index(){
      return Inertia::render('Posts',[
        'posts' => \App\Models\Post::orderBy('date', 'DESC')->get()->map(function ($i){
           return [
               'id' => $i->id,
               'image' => $i->image,
               'title' => $i->title,
               'short_description' =>Str::limit($i->short_description,80),
               'like' => $i->like,
               'description' => $i->description,
               'date' => $i->date,
           ];
        }),
      ]);
    }

}
