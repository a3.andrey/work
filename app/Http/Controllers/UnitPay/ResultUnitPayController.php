<?php

namespace App\Http\Controllers\UnitPay;

use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\UnitpayPayment;
use Illuminate\Http\Request;
use RekkoPavel\UnitPay\Traits\ValidateTrait;


/**
 *
 */
class ResultUnitPayController extends Controller
{
    use ValidateTrait;

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Foundation\Application
     */
    public function successURL(Request $request)
    {

        if (!$this->validateRequest($request)) {

            abort(403, 'Доступ запрещен');
        }

        $order = Order::where('id', $request->input('account'))->first();
        $payment = UnitpayPayment::where('unitpay_id', $request->input('paymentId'))->first();

        if (is_null($order) || is_null($payment)) {

            return view('payment-success', ['message' => 'Заказ или платеж не найден', 'order' => '', 'payment' => '']);
        }
        return view('payment-success', ['message' => 'Платеж произведен!', 'order' => $order->id, 'payment' => $payment->id]);

    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Foundation\Application
     */
    public function failURL(Request $request)
    {
        if (!$this->validateRequest($request)) {

            abort(403, 'Доступ запрещен');
        }

        $order = Order::where('id', $request->input('account'))->first();
        $payment = UnitpayPayment::where('unitpay_id', $request->input('paymentId'))->first();

        if (is_null($order) || is_null($payment)) {

            return view('payment-success', ['message' => 'Заказ или платеж не найден', 'order' => '', 'payment' => '']);
        }
        return view('payment-fail', ['message' => 'Платеж не был произведен!', 'order' => $order->id, 'payment' => $payment->id]);

    }

    /**
     * @param Request $request
     * @return string
     */
    public function backURL(Request $request)
    {
        if (!$this->validateRequest($request)) {

            abort(403, 'Доступ запрещен');
        }

        return route('orders');

    }


}
