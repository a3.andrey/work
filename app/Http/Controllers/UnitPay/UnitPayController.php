<?php

namespace App\Http\Controllers\UnitPay;

use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\UnitpayPayment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use RekkoPavel\UnitPay\Facades\UnitPay;

class UnitPayController extends Controller
{


    /**
     * Search the order in your database and return that order
     * to paidOrder, if status of your order is 'paid'
     *
     * @param Request $request
     * @param $order_id
     * @return bool|mixed
     */
    public function searchOrder(Request $request, $order_id)
    {
        $order = Order::where('id', $order_id)->first();

        if ($order) {
            $order['_orderSum'] = $order->sum;
            $order['_orderStatus'] = ($order->status == '1') ? 'paid' : false;

            return $order;
        }

        return false;
    }

    /**
     * When paymnet is check, you can paid your order
     *
     * @param Request $request
     * @param $order
     * @return bool
     */
    public function paidOrder(Request $request, $order)
    {
        DB::beginTransaction();
        try {
            $payment = new UnitpayPayment();
            $payment->unitpay_id = $request->input('params.unitpayId');
            $payment->account = $request->input('params.account');
            $payment->sum = $request->input('params.orderSum');
            $payment->user_id = $order->user_id;
            $payment->order_id = $order->id;
            $payment->status = '1';
            $order->status = '1';
            $payment->save();
            $order->save();
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            Log::channel('unitpay')->critical('Ошибка при записи платежа, заказ - '. $request->input('params.account'));

        }


        return true;
    }

    /**
     * Start handle process from route
     *
     * @param Request $request
     * @return mixed
     */
    public function handlePayment(Request $request)
    {
        $errorMessage = $request->input('params.errorMessage') ?? 'OK';

        Log::channel('unitpay')->info('Получен запрос от платежной системы: метод - ' . $request->input('method') .
            ' заказ - ' . $request->input('params.account') . ' ошибка - ' . $errorMessage);

        return UnitPay::handle($request);
    }
}
