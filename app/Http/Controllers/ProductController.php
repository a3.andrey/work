<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductRequest;
use App\Http\Resources\CreateItemResource;
use App\Http\Resources\ItemEditResource;
use App\Models\Category;
use App\Models\Game;
use App\Models\Item;
use App\Models\Notification;
use App\Models\Product;
use App\Traits\ProductTrait;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;

class ProductController extends Controller
{
    use ProductTrait;

    public function create($item = null, $category=null, $product=null){

        $game = null;
        $price = null;
        $count = null;
        $price_min = null;
        $features_icon = [];
        $features_text = [];
        $features_number = [];
        $features_color = null;


        if($item&&$category){

            $item = Item::find($item);
            $category = Category::find($category);
            if ($item){
                $item = new CreateItemResource($item);
                $game = $item->game;
            }
        }

        if($product){
            $product = Product::find($product);
            if($product){
                $price = $product->price;
                $count = $product->count;
                $price_min = $product->price_min;
                $features_icon = \App\Http\Resources\IconFeatures::collection($product->feature_icons);
                $features_text = $item->textFeatures->pluck('id');
                $features_number =  $product->features_numbers->map(function ($num){
                    $num->value = $num->pivot->value;
                    $num->check = true;
                    return $num;
                });
                $features_color = $product->features_color;
            }
        }

        return  Inertia::render('Dashboard/LotsCreate',[
            'title' => 'Создать лот',
            'url_submit' => route('dashboard.lots.store'),
            'games' => Game::getItemsGames()->get()->chunk(2),
            'game' => $game,
            'categories' => Category::all(),
            'category' => $category,
            'item' => $item?new ItemEditResource($item):null,
            'price' => $price,
            'count' => $count,
            'price_min' => $price_min,
            'features_icon' => $features_icon,
            'features_text' => $features_text,
            'features_number' => $features_number,
            'features_color' => $features_color,
        ]);
    }

    public function replicate(Product $product){
        //copy attributes from original model
        $newRecord = $product->replicate();
        //save model before you recreate relations (so it has an id)
        $newRecord->push();
        //reset relations on EXISTING MODEL (this way you can control which ones will be loaded
        $product->relations = [];
        //load relations on EXISTING MODEL
        $product->load('feature_icons', 'features_texts','features_numbers');
        //re-sync the child relationships
        $relations = $product->getRelations();

        foreach ($relations as $relationName => $values) {
            $newRecord->{$relationName}()->sync($values);
        }
       return redirect()->back();
    }

    public function edit(Product $product){
        return Inertia::render('Dashboard/LotsCreate',[
            'title' => 'Редактировать лот',
            'url_submit' => route('dashboard.lots.update',$product),
            'product' =>  $product,
            'game' =>  $product->game,
            'games' => Game::getItemsGames()->get()->chunk(2),
            'categories' => Category::all(),
            'category' => $product->category,
            'item' => new ItemEditResource($product->item),
            'features_icon' => \App\Http\Resources\IconFeatures::collection($product->feature_icons) ,
            'features_text' => $product->features_texts->map(function ($item){
                return [
                    'id' => $item->id,
                    'created_at' => $item->created_at,
                    'item_id' => $item->item_id,
                    'name' => $item->name,
                    'updated_at' => $item->updated_at,
                    'value' => $item->value,
                ];
            }),
            'features_number' => $product->features_numbers->map(function ($item){
                $item->value = $item->pivot->value;
                $item->check = true;
                return $item;
            }),
            'features_color' => $product->features_color,
            'qtu' => $product->count,
            'price' => $product->price,
            'min_price' => $product->price_min,
        ]);
    }

    public function update(ProductRequest $productUpdateRequest,Product $product){

        $product->update([
            'count' => request('qtu'),
            'price' => request('price'),
            'price_min' => request('min_price'),
            'game_id' => Arr::get(request('game'),'id'),
            'category_id' => Arr::get(request('category'),'id'),
            'item_id' => Arr::get(request('item'),'id'),
            'item_features_colors_id' => request('features_color') ?
                Arr::get(request('features_color'),'id') :
                null,
        ]);

        $features_textArr = [];
        foreach (request('features_text')  as $features_text){
            $features_textArr[] = $features_text['id'];
        }

        $product->features_texts()->sync($features_textArr);

        return redirect()->route('dashboard.lots.index');
    }


    //
    public function store(ProductRequest $productRequest){
        // TODO: Implement handle() method.

        $product = Product::create([
            'count' => request('qtu'),
            'price' => request('price'),
            'price_min' => request('min_price'),
            'user_id' => Auth::id(),
            'game_id' => Arr::get(request('game'),'id'),
            'category_id' => Arr::get(request('category'),'id'),
            'item_id' => Arr::get(request('item'),'id'),
            'item_features_colors_id' => request('features_color') ?
                Arr::get(request('features_color'),'id') :
                null,
        ]);

        if(request('features_icon')){
            $arrIcons = array_map(fn($item) => Arr::get($item,'id') , request('features_icon'));

            $product->feature_icons()->attach($arrIcons);
        }

        if(request('features_text')){
            $product->features_texts()->attach(request('features_text'));
        }

        if(request('features_number')){
           array_map(function($item) use ($product){
                if($item['check'] && $item['value'] !== null){
                    $product->features_numbers()->attach(
                        Arr::get($item,'id'),['value' => Arr::get($item,'value')]
                    );
                }
            },request('features_number'));
        }

        Notification::create([
            'description' => 'Добавлен новый лот',
            'icon' => 'message',
            'link' => route('dashboard.lots.create'),
            'user_id' => Auth::id(),
        ]);

        return redirect()->route('dashboard.lots.index');
    }
}
