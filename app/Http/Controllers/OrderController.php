<?php

namespace App\Http\Controllers;

use App\Models\OrderItem;
use \App\Models\Order;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;
use RekkoPavel\UnitPay\Facades\UnitPay;

class OrderController extends Controller
{
    public function __invoke()
    {

//        $order = Order::create([
//            'sum' => \request('total'),
//            'user_id' => Auth::id(),
//        ]);

//        foreach (\request('products') as $product){
//            OrderItem::create([
//                'item' => $product,
//                'order_id' => $order->id
//            ]);
//        }

        $url = UnitPay::getPayUrl(100, 23, 'text@mail.com', 'test', config('app.currency'));
        //$redirect = UnitPay::redirectToPayUrl($order->amount, $order->id, $order->user->email, $order->description, config('app.currency'));
        return Inertia::location($url);
        return view('order', ['order' => $order, 'url' => $url]);
    }
}
