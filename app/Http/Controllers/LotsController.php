<?php

namespace App\Http\Controllers;

use App\Http\Resources\ColorFeatures;
use App\Http\Resources\IconFeatures;
use App\Http\Resources\NumberFeatures;
use App\Http\Resources\TextFeatures;
use App\Models\Category;
use Inertia\Inertia;
class LotsController extends Controller
{
    //
    public function __invoke(\App\Models\Game $game,Category $category, \App\Models\Item $item){

        $filters = [
            'price' => [
                'min' => null,
                'max' => null,
            ],
            'iconFeatures' => IconFeatures::collection($item->iconFeatures),
            'textFeatures' => $item->textFeatures->map(function ($i){
                return [
                    'id' => $i->id,
                    'name' => $i->name.' - '.$i->value,
                ];
            }),
            'colorFeatures' => ColorFeatures::collection($item->colorFeatures),
            'numberFeatures' => NumberFeatures::collection($item->numberFeatures),
        ];

        $items =\App\Http\Resources\ProductResource::collection($item
            ->products()
            ->iconFilter(request('huge'))
            ->textFilter(request('text'))
            ->colorFilter(request('color'))
            ->paginate(1000)
        );

        if(request()->wantsJson()){
            return $items;
        }

       return  Inertia::render('Lots',[
            'game' => $game,
            'category' => $category,
            'filters' => $filters,
            'item' => $item,
            'lots' => $items,
        ]);
    }
}
