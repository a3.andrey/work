<?php

namespace App\Services;

use Illuminate\Support\Facades\Storage;

class ImagePlaceholder
{
    private const URL = 'https://via.placeholder.com';
    private $width;
    private $height;
    private $background;
    private $colorFont;
    private $urlImageGenerated;

    public function __construct($width=500,$height=200,$background='258DC8',$colorFont='E0F6FD'){
        $this->width = $width;
        $this->height = $height;
        $this->background = $background;
        $this->colorFont = $colorFont;
        $this->generateUrlLink();
    }

    protected function generateUrlLink(){
        $this->urlImageGenerated = self::URL.'/'.$this->width.'x'.$this->height.'/'.$this->background.'/'.$this->colorFont;
        return $this;
    }

    public function width($number){
        $this->width = $number;
        return $this;
    }

    public function height($number){
        $this->height = $number;
        return $this;
    }

    public function get(){
        return $this->urlImageGenerated;
    }

    public function put(string $prefix='images'){

        $content = file_get_contents($this->urlImageGenerated);

        $path = '/public/'.$prefix.'/'.time().'.jpg';

        Storage::put($path, $content);

        return $path;
    }

}
