<?php

namespace App\Services;
use Illuminate\Support\Arr;
use Ixudra\Curl\Facades\Curl;
class SearchService
{
    private const URL = 'https://speller.yandex.net/services/spellservice.json';

    private const METHOD = 'checkText';
    private function generateUrl():string{
        return self::METHOD.'/'.self::METHOD;
    }

    private function correctionWord(){

    }

    private function prepareWord(string $word){
        return trim($word);
    }

    private function transformationResponse($response):string{
       return  Arr::get($response?->s?:[],0);
    }

    public function search(string $word){
        $response = Curl::to('https://speller.yandex.net/services/spellservice.json/checkText')
            ->withData( array( 'text' => $this->prepareWord($word)) )
            ->asJson()
            ->get();

        if(count($response)){
            $firstResp = Arr::get($response,0);
            if($firstResp){
                return $this->transformationResponse($firstResp);
            }
        }
        return null;
    }
}
