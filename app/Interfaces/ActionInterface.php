<?php
namespace App\Interfaces;

interface ActionInterface
{
    /**
     * @return mixed
     */
    public function handle();
}
