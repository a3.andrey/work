<?php

namespace App\Interfaces;

interface SearchInterface
{
    /**
     * @return array
     *
     */
    public function handle():array;
}
