<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Storage;

class Post extends Model
{
  use HasFactory;
   protected $fillable = [
    'image',
    'title',
    'date',
    'short_description',
    'like',
    'tags',
    'description',
   ];

    /**
     * Get the user's first name.
     */
    protected function image(): Attribute
    {
        return Attribute::make(
            get: fn (string|null $value) => Storage::url($value),
            set: fn($val) => $val,
        );
    }

//    /**
//     * Get the user's first name.
//     */
//    protected function description(): Attribute
//    {
//        return Attribute::make(
//            get: fn (string|null $value) => empty($value) ? null : nl2br($value),
//        );
//    }

    /**
     * Get the user's first name.
     */
    protected function date(): Attribute
    {
        return Attribute::make(
            get: fn (string|null $value) => Carbon::parse($value)->format('d.m.Y'),
        );
    }

   public function tags(){
    return $this->belongsToMany(Tag::class, 'post_tag', 'post_id', 'tag_id' );
   }



}
