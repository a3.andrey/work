<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function game(){
        return $this->belongsTo(Game::class,'game_id');
    }

    public function item(){
        return $this->belongsTo(Item::class,'item_id');
    }

    public function category(){
        return $this->belongsTo(Category::class,'category_id');
    }

    public function categories(){
        return $this->belongsToMany(Category::class,'item_category','product_id','category_id');
    }

    public function feature_icons(){
        return $this->belongsToMany(ItemFeaturesIcon::class,'product_feature_icon','product_id','item_feature_icon_id');
    }

    public function features_texts(){
        return $this->belongsToMany(ItemFeaturesText::class,'product_feature_text','product_id','item_feature_text_id')
            ->withPivot('value');
    }

    public function features_numbers(){
        return $this->belongsToMany(ItemFeaturesNumber::class,'product_feature_number','product_id','item_features_number_id')
            ->withPivot('value');
    }

    public function features_color(){
        return $this->belongsTo(ItemFeaturesColor::class,'item_features_colors_id');
    }

    public function scopeIconFilter($query,array|null $icons){
        if(is_array($icons) && count($icons) > 0 ){
            return $query->whereHas('feature_icons', function($q)use($icons) {
                $q->whereIn('item_feature_icon_id',  $icons);
            });
        }
        return $query;
    }

    public function scopeTextFilter($query,array|null $text){
        if(is_array($text) && count($text) > 0 ){
            return $query->whereHas('features_texts', function($q)use($text) {
                $q->whereIn('item_feature_text_id',  $text);
            });
        }
        return $query;
    }

    public function scopeColorFilter($query,array|null $color){
        if(is_array($color) && count($color) > 0 ){
            $query->whereIn('item_features_colors_id',$color);
        }
        return $query;
    }


}
