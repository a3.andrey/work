<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\Notifications;

class Icon extends Model
{
    use HasFactory;

    protected $guarded = [];

    /**
     * Get the notification that owns the Icon
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function notification(): BelongsTo
    {
        return $this->belongsTo(Notifications::class);
    }
}
