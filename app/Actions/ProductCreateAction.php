<?php

namespace App\Actions;

use App\Models\Category;
use App\Models\Game;
use App\Models\Item;

class ProductCreateAction
{
    public function handle(){
        $items = Item::all();
        $item = null;
        if(request('item')){
            $itemsReq = Item::where('id',request('item'))
                ->where('game_id',request('game'))
                ->where('category_id',request('category'))
                ->get()
                ->map(function ($item){
                return [
                    'id' => $item->id,
                    'name' => $item->name,
                    'iconFeatures' => \App\Http\Resources\IconFeatures::collection($item->iconFeatures) ,
                    'textFeatures' => $item->textFeatures->chunk(2),
                    'colorFeatures' => $item->colorFeatures->chunk(2),
                    'numberFeatures' => $item->numberFeatures->chunk(3),
                ];
            });

            $item = $itemsReq->first();
        }

        return [
            'title' => 'Создать лот',
            'games' => Game::getItemsGames()->get()->chunk(2),
            'game' => Game::find(request('game')),
            'categories' => Category::whereIn('id',$items->pluck('category_id')->toArray())->get(),
            'category' => Category::find(request('category')),
            'item' => $item,
        ];
    }

}
