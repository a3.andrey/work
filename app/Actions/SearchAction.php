<?php

namespace App\Actions;

use App\Interfaces\SearchInterface;

class SearchAction implements SearchInterface
{
    public string $search;

    public function __construct(){
        $this->search = request('search');
    }

    /**
     * @return array|string
     */
    public function handle():array{
        return response()->json();
    }
}
