<?php

namespace App\Actions;

use App\Http\Requests\ProductRequest;
use App\Interfaces\ActionInterface;
use App\Models\Notification;
use App\Models\Product;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;

class ProductStoreAction
{

    public function handle(ProductRequest $productRequest)
    {
        // TODO: Implement handle() method.
        $product = Product::create([
            'count' => request('qtu'),
            'price' => request('price'),
            'price_min' => request('min_price'),
            'user_id' => Auth::id(),
            'game_id' => Arr::get(request('game'),'id'),
            'category_id' => Arr::get(request('category'),'id'),
            'item_id' => Arr::get(request('item'),'id'),
            'item_features_colors_id' => Arr::get(request('features_color'),'id'),
        ]);

        $arrIcons = array_map(fn($item) => Arr::get($item,'id') , request('features_icon'));

        $arrTexts = array_map(fn($item) => Arr::get($item,'id') , request('features_text'));

        $features_numbers = array_map(function($item) use($product){
            if($item['check'] && $item['value'] !== null){
                $product->features_numbers()->attach(
                    Arr::get($item,'id'),['value' => Arr::get($item,'value')]
                );
            }
        },request('features_number'));


        $product->feature_icons()->attach($arrIcons);

        $product->features_texts()->attach($arrTexts);

       Notification::create([
           'description' => 'Добавлен новый лот',
           'icon' => 'message',
           'link' => route('dashboard.lots.create'),
           'user_id' => Auth::id(),
       ]);

        return redirect()->route('dashboard.lots.index');
    }

}
