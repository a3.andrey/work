<?php

namespace App\Actions;

use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;

class LoginGoogleCallback
{
    public function __construct(){
        try {

            $user = Socialite::driver('google')->user();

            $finduser = User::where('google_id', $user->id)->first();

            if ( $finduser ) {

                Auth::login($finduser);

               return  redirect()->intended(RouteServiceProvider::HOME);

            } else {
                $newUser = User::create([
                    'name' => $user->name,
                    'email' => $user->email,
                    'google_id'=> $user->id,
                    'password' => 'dummypass'// you can change auto generate
                    // password here and send it via email but you need to add checking
                    // that the user need to change the password for security reasons
                ]);

                Auth::login($newUser);

                return redirect()->intended('/');
            }

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function redirect(){
       return  redirect()->intended(RouteServiceProvider::HOME);
    }
}
