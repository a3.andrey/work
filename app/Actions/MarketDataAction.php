<?php

namespace App\Actions;

use App\Models\Game;
use App\Models\Item;
use \Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Arr;

class MarketDataAction
{
    public static function handle(Game $game){
       $items = $game->items;
       if(request('price')){
           $min = Arr::get(request('price'),0);
           $max = Arr::get(request('price'),1);
           $items->map(fn($item) => $item->price = $item->price);
           $items = $items->filter(fn($item) => (int)$item->price >= (int)$min && (int)$item->price <= (int)$max);
       }

       return self::paginateGenerate($items);
    }



    private static function paginateGenerate($collection){
       $arr = $collection->pluck('id');
       return Item::whereIn('id',$arr)->paginate(1000);
    }
}
