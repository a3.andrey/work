<?php

namespace App\Admin\Http\Controllers;

use App\Admin\Http\Requests\PostRequest;
use App\Http\Requests\GameRequest;
use App\Models\Game;
use App\Models\GameTag;
use App\Models\Tag;
use Inertia\Inertia;

class GameController
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return Inertia::render('Admin/Games/Index',
            [
                'games' => \App\Models\Game::orderBy('id', 'DESC')->get(),
            ]
        );
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return Inertia::render('Admin/Games/Create',[
            'tags' => GameTag::all()->pluck('name','id'),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(GameRequest $postRequest)
    {
        //
        $game = Game::create($postRequest->only([
            'name',
            'image',
        ]));

        if(\request()->file('image')){
            $game->image = \request('image')->store('image');
            $game->save();
        }

        return redirect()->route('admin.games.index');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Game $game)
    {
        return Inertia::render('Admin/Games/Edit',
            [
                'game' => $game,
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Game $game)
    {
        //
        $game->update(\request()->only('name'));

        if(\request()->file('image')){
            $game->image = \request('image')->store('image');
            $game->save();
        }

        return redirect()->route('admin.games.index');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Game $game)
    {
        //
        $game->delete();
        return redirect()->route('admin.games.index');
    }
}
