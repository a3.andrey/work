<?php



namespace App\Admin\Http\Controllers;

use App\Admin\Http\Requests\PostRequest;
use App\Http\Controllers\Controller;
use App\Models\Post;
use Illuminate\Http\Request;
use Inertia\Inertia;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
      return Inertia::render('Admin/Posts/Index',
      [
        'posts' => \App\Models\Post::orderBy('id', 'DESC')->get(),
      ]
    );
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
      return Inertia::render('Admin/Posts/Create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(PostRequest $postRequest)
    {
        //
       $post = Post::create($postRequest->only([
            'title',
            'date',
            'short_description',
            'description',
        ]));

        if(\request()->file('image')){
            $post->image = \request('image')->store('image');
            $post->save();
        }

        return redirect()->route('admin.posts.index');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Post $post)
    {
        return Inertia::render('Admin/Posts/Edit',
            [
                'post' => $post,
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Post $post)
    {
        //
        $post->update($request->only([
            'title',
            'date',
            'short_description',
            'description',
        ]));

        if($request->file('image')){

            $post->image = \request('image')->store('image');
            $post->save();
        }


        return redirect()->route('admin.posts.index');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Post $post)
    {
        //
        $post->delete();
        return redirect()->back();
    }
}
