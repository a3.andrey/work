<?php

namespace App\Admin\Http\Controllers;

use App\Http\Resources\CategoryResource;
use App\Http\Resources\GameResource;
use App\Models\Category;
use App\Models\Game;
use App\Models\ItemFeaturesIcon;
use Illuminate\Support\Arr;
use Inertia\Inertia;

class GraphicController
{
    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
        $items = ItemFeaturesIcon::all();
        return Inertia::render('Admin/Graphics/Create',[
            'items' => $items,
        ]);
    }

    public function store(){
        foreach (request()->graphic?:[] as $graphic){
            if(Arr::get($graphic,'text') &&
                Arr::get($graphic,'img')
            ){
                ItemFeaturesIcon::create([
                    'name' => Arr::get($graphic,'text'),
                    'icon' => Arr::get($graphic,'img')->store('avatars'),
                ]);
            }
        }
        return redirect()->route('admin.items.create');
    }
}
