<?php

namespace App\Admin\Http\Requests;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ItemRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'name' => ['required','string', 'max:255'],
            'image' => ['required'],
            'game' => ['required'],
            'categories' => ['required'],
            'texts' => ['array','min:1'],
            'graphic' => ['array','min:1'],
            'graphic.*.text' => ['required','string', 'max:255'],
            'graphic.*.img' => ['required','image', 'max:500000'],
            'numbers' => ['array','min:1'],
            'numbers.*.text' => ['required','string', 'max:255'],
            'numbers.*.min' => ['required','string', 'max:255'],
            'numbers.*.max' => ['required','string', 'max:255'],
            'rares' => ['array','min:1'],
            'rares.*.name' => ['required','string', 'max:255'],
            'rares.*.color' => ['required'],
        ];
    }
}
