<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('product_feature_number', function (Blueprint $table) {
            $table->id();

            $table->bigInteger('product_id')->unsigned()->index()->nullable();
            $table->foreign('product_id')
                ->references('id')->on('products')->onDelete('cascade');

            $table->bigInteger('item_features_number_id')->unsigned()->index()->nullable();
            $table->foreign('item_features_number_id')
                ->references('id')->on('item_features_numbers')->onDelete('cascade');

            $table->integer('value');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('product_feature_number');
    }
};
