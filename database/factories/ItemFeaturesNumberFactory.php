<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\ItemFeaturesNumber>
 */
class ItemFeaturesNumberFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $randTo = rand(1,1000);
        return [
            //
            'name' => fake()->name(),
            'to' => $randTo,
            'from' => rand($randTo,$randTo*10),
        ];
    }
}
