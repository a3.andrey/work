<?php

namespace Database\Factories;

use App\Services\ImagePlaceholder;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\ItemFeaturesIcon>
 */
class ItemFeaturesIconFactory extends Factory
{

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $image = new ImagePlaceholder(35,35);
        return [
            //
            'name' => fake()->name,
            'icon' => $image->put(),
        ];
    }
}
