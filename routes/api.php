<?php

use App\Models\Avatar;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

//Route::post('register/email-verification',fn(\App\Actions\RegisterEmailVerificationAction $emailVerificationAction) => $emailVerificationAction->sendEmail(request('email')))
//    ->name('register.email-verification');

Route::post('register/check/nickname',fn(\App\Http\Requests\Auth\NicknameRequest $nicknameRequest) => $nicknameRequest)
    ->name('check.nickname');

Route::post('register/check/email',fn(\App\Http\Requests\Auth\EmailRequest $emailRequest) => $emailRequest)
    ->name('check.email');

Route::post('register/check/password',fn(\App\Http\Requests\Auth\PasswordRequest $emailRequest) => $emailRequest)
    ->name('check.password');

Route::post('register/check/password_confirmation',fn(\App\Http\Requests\Auth\PasswordConfirmed $emailRequest) => $emailRequest)
    ->name('check.password_confirmation');

Route::get('avatars', function() {
    return Avatar::orderByDesc('id')->limit(12)->get();
})->name('get.avatars');
